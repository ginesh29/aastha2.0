﻿using Aastha.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Aastha.Entities.Data
{
    public class ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : DbContext(options)
    {
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Ipd>().HasIndex(u => u.UniqueId).IsUnique();
            modelBuilder.Entity<Patient>().Property(e => e.Firstname).HasConversion(v => v.Trim(), v => v.Trim());
            modelBuilder.Entity<Patient>().Property(e => e.Middlename).HasConversion(v => v.Trim(), v => v.Trim());
            modelBuilder.Entity<Patient>().Property(e => e.Fathername).HasConversion(v => v.Trim(), v => v.Trim());
            modelBuilder.Entity<Patient>().Property(e => e.Lastname).HasConversion(v => v.Trim(), v => v.Trim());
            modelBuilder.Entity<Payment>().HasOne(p => p.Ipd).WithMany(i => i.Payments).HasForeignKey(p => p.IpdId).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<FormFDetail>().Property(m => m.Children).HasConversion(value => JsonConvert.SerializeObject(value), serializedValue => JsonConvert.DeserializeObject<List<Child>>(serializedValue));
            modelBuilder.Entity<FormFDetail>().Property(e => e.DiagnosisProcedure)
            .HasConversion(v => string.Join(',', v),
                v => v.Split(',', StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray()
            );
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<Opd> Opds { get; set; }
        public DbSet<Ipd> Ipds { get; set; }
        public DbSet<Appointment> Appointments { get; set; }
        public DbSet<Lookup> Lookups { get; set; }
        public DbSet<Delivery> Deliveries { get; set; }
        public DbSet<Operation> Operations { get; set; }
        public DbSet<IpdLookup> IpdLookups { get; set; }
        public DbSet<Charge> Charges { get; set; }
        public DbSet<Prescription> Prescriptions { get; set; }
        public DbSet<PrescriptionMedicine> PrescriptionMedicines { get; set; }
        public DbSet<FormFDetail> FormFDetails { get; set; }
        public DbSet<Payment> Payments { get; set; }
    }
}
