﻿namespace Aastha.Entities.StoredProcedure
{
    public class Sp_GetPatientsHistories
    {
        public long Id { get; set; }
        public long PatientId { get; set; }
        public int Type { get; set; }
        public string PatientName { get; set; }
        public DateTime? Date { get; set; }
    }
}
