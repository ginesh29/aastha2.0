﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Aastha.Entities.Models
{
    public class PrescriptionMedicine : BaseEntity
    {
        public int Days { get; set; }
        public int Qty { get; set; }
        [MaxLength(50)]
        public string MedicineType { get; set; }
        [MaxLength(50)]
        public string MedicineName { get; set; }
        [MaxLength(200)]
        public string MedicineInstruction { get; set; }
        public long PrescriptionId { get; set; }
        [ForeignKey("PrescriptionId")]
        public Prescription Prescription { get; set; }
    }
}
