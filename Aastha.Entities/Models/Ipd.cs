﻿using Aastha.Common;
using System.ComponentModel.DataAnnotations.Schema;

namespace Aastha.Entities.Models
{
    public class Ipd : BaseEntity
    {
        public long UniqueId { get; set; }
        public IpdTypeEnum Type { get; set; }
        public RoomTypeEnum RoomType { get; set; }
        public DateTime AddmissionDate { get; set; }
        public DateTime? DischargeDate { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Discount { get; set; }
        public long PatientId { get; set; }
        [ForeignKey("PatientId")]
        public Patient Patient { get; set; }
        public Delivery DeliveryDetail { get; set; }
        public Operation OperationDetail { get; set; }
        public ICollection<Charge> Charges { get; set; }
        public ICollection<IpdLookup> IpdLookups { get; set; }
        public ICollection<Payment> Payments { get; set; }
    }
}
