﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Aastha.Entities.Models
{
    public class Prescription : BaseEntity
    {
        public DateTime Date { get; set; }
        public long PatientId { get; set; }
        [ForeignKey("PatientId")]
        public Patient Patient { get; set; }
        [MaxLength(1000)]
        public string ClinicalDetail { get; set; }
        public int FollowupType { get; set; }
        public DateTime? FollowupDate { get; set; }
        [MaxLength(500)]
        public string Advices { get; set; }
        public IEnumerable<PrescriptionMedicine> PrescriptionMedicines { get; set; }
    }
}
