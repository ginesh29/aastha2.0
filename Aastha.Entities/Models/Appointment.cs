﻿using Aastha.Common;
using System.ComponentModel.DataAnnotations.Schema;

namespace Aastha.Entities.Models
{
    public class Appointment : BaseEntity
    {
        public DateTime Date { get; set; }
        public AppointmentTypeEnum Type { get; set; }

        public long PatientId { get; set; }
        [ForeignKey("PatientId")]
        public Patient Patient { get; set; }
    }
}
