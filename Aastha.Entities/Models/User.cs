﻿using System.ComponentModel.DataAnnotations;

namespace Aastha.Entities.Models
{
    public class User : BaseEntity
    {
        [MaxLength(50)]
        public string Firstname { get; set; }
        [MaxLength(50)]
        public string Middlename { get; set; }
        [MaxLength(50)]
        public string Lastname { get; set; }
        [MaxLength(50)]
        public string Username { get; set; }
        [MaxLength(100)]
        public string Password { get; set; }
        public bool IsSuperAdmin { get; set; }
        [MaxLength(50)]
        public string FormFUsername { get; set; }
        [MaxLength(100)]
        public string FormFPassword { get; set; }
    }
}
