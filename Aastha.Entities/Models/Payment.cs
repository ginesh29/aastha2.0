﻿using Aastha.Common;
using System.ComponentModel.DataAnnotations.Schema;

namespace Aastha.Entities.Models
{
    public class Payment : BaseEntity
    {
        public long? IpdId { get; set; }
        [ForeignKey("IpdId")]
        public Ipd Ipd { get; set; }
        public long? OpdId { get; set; }
        [ForeignKey("OpdId")]
        public Opd Opd { get; set; }
        public DateTime PaymentDate { get; set; }
        public PaymentModeEnum PaymentMode { get; set; }
        public decimal Amount { get; set; }
        public DeptTypeEnum Dept { get; set; }
    }
}
