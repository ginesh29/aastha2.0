﻿using Aastha.Common;
using System.ComponentModel.DataAnnotations.Schema;

namespace Aastha.Entities.Models
{
    public class Delivery : BaseEntity
    {
        public long IpdId { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan Time { get; set; }
        public ChildGenderEnum Gender { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal BabyWeight { get; set; }

        [ForeignKey("IpdId")]
        public Ipd Ipd { get; set; }
    }
}
