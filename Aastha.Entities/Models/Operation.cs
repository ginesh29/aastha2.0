﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Aastha.Entities.Models
{
    public class Operation : BaseEntity
    {
        public long IpdId { get; set; }
        public DateTime Date { get; set; }

        [ForeignKey("IpdId")]
        public Ipd Ipd { get; set; }
    }
}
