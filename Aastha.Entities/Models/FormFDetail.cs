﻿using Aastha.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Aastha.Entities.Models
{
    public class FormFDetail : BaseEntity
    {
        public DateTime Date { get; set; }
        public DateTime LMPDate { get; set; }
        public long PatientId { get; set; }
        [ForeignKey("PatientId")]
        public Patient Patient { get; set; }
        [MaxLength(200)]
        public List<Child> Children { get; set; }
        [MaxLength(10)]
        public int[] DiagnosisProcedure { get; set; }
        [MaxLength(200)]
        public string DiagnosisResult { get; set; }
        public bool ThumbImpression { get; set; }
        [MaxLength(50)]
        public string RelativeName { get; set; }      
        public GenderEnum RelativeGender { get; set; }
        [MaxLength(20)]
        public string RelativeRelation { get; set; }
        public int RelativeAge { get; set; }
        [MaxLength(100)]
        public string RelativeAddress { get; set; }
        [MaxLength(20)]
        public string RelativeMobile { get; set; }
        public bool Submitted { get; set; }
    }
    public class Child
    {
        public int No { get; set; }
        public int Type { get; set; }
        public int AgeYr { get; set; }
        public int AgeMn { get; set; }
    }
}
