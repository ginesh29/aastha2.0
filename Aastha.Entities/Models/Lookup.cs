﻿using Aastha.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Aastha.Entities.Models
{
    public class Lookup : BaseEntity
    {
        [MaxLength(100)]
        public string Name { get; set; }
        public LookupTypeEnum Type { get; set; }
        public long? ParentId { get; set; }

        [ForeignKey("ParentId")]
        public Lookup Parent { get; set; }
        public ICollection<Lookup> Children { get; set; }
    }
}
