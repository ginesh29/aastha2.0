﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Aastha.Entities.Models
{
    public class Patient : BaseEntity
    {
        [MaxLength(50)]
        public string Firstname { get; set; }
        [MaxLength(50)]
        public string Middlename { get; set; }
        [MaxLength(50)]
        public string Fathername { get; set; }
        [MaxLength(50)]
        public string Lastname { get; set; }
        [NotMapped]
        public string FullName => $"{Firstname} {Middlename}{(!string.IsNullOrEmpty(Fathername) ? $"({Fathername})" : string.Empty)} {Lastname}";
        [NotMapped]
        public string Name => $"{Firstname} {Middlename} {Lastname}";
        [MaxLength(20)]
        public string Mobile { get; set; }
        public int Age { get; set; }
        public long? CityId { get; set; }

        [ForeignKey("CityId")]
        public Lookup City { get; set; }
        public long? DistId { get; set; }

        [ForeignKey("DistId")]
        public Lookup Dist { get; set; }
        public long? TalukaId { get; set; }

        [ForeignKey("TalukaId")]
        public Lookup Taluka { get; set; }
        [MaxLength(100)]
        public string Address1 { get; set; }
        [MaxLength(100)]
        public string Address2 { get; set; }
        public DateTime? BirthDate { get; set; }
    }
}
