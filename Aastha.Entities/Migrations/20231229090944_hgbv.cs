﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Aastha.Entities.Migrations
{
    /// <inheritdoc />
    public partial class hgbv : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Patients_Lookups_DistId",
                table: "Patients");

            migrationBuilder.DropForeignKey(
                name: "FK_Patients_Lookups_TalukaId",
                table: "Patients");

            migrationBuilder.DropIndex(
                name: "IX_Patients_DistId",
                table: "Patients");

            migrationBuilder.DropIndex(
                name: "IX_Patients_TalukaId",
                table: "Patients");

            migrationBuilder.DropColumn(
                name: "Address1",
                table: "Patients");

            migrationBuilder.DropColumn(
                name: "Address2",
                table: "Patients");

            migrationBuilder.DropColumn(
                name: "ChildrenDetail",
                table: "Patients");

            migrationBuilder.DropColumn(
                name: "DistId",
                table: "Patients");

            migrationBuilder.DropColumn(
                name: "TalukaId",
                table: "Patients");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Address1",
                table: "Patients",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address2",
                table: "Patients",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ChildrenDetail",
                table: "Patients",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DistId",
                table: "Patients",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "TalukaId",
                table: "Patients",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Patients_DistId",
                table: "Patients",
                column: "DistId");

            migrationBuilder.CreateIndex(
                name: "IX_Patients_TalukaId",
                table: "Patients",
                column: "TalukaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Patients_Lookups_DistId",
                table: "Patients",
                column: "DistId",
                principalTable: "Lookups",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Patients_Lookups_TalukaId",
                table: "Patients",
                column: "TalukaId",
                principalTable: "Lookups",
                principalColumn: "Id");
        }
    }
}
