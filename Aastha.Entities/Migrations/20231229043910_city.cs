﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Aastha.Entities.Migrations
{
    /// <inheritdoc />
    public partial class city : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Patients_Lookups_AddressId",
                table: "Patients");

            migrationBuilder.RenameColumn(
                name: "AddressId",
                table: "Users",
                newName: "CityId");

            migrationBuilder.RenameColumn(
                name: "AddressId",
                table: "Patients",
                newName: "CityId");

            migrationBuilder.RenameIndex(
                name: "IX_Patients_AddressId",
                table: "Patients",
                newName: "IX_Patients_CityId");

            migrationBuilder.AddForeignKey(
                name: "FK_Patients_Lookups_CityId",
                table: "Patients",
                column: "CityId",
                principalTable: "Lookups",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Patients_Lookups_CityId",
                table: "Patients");

            migrationBuilder.RenameColumn(
                name: "CityId",
                table: "Users",
                newName: "AddressId");

            migrationBuilder.RenameColumn(
                name: "CityId",
                table: "Patients",
                newName: "AddressId");

            migrationBuilder.RenameIndex(
                name: "IX_Patients_CityId",
                table: "Patients",
                newName: "IX_Patients_AddressId");

            migrationBuilder.AddForeignKey(
                name: "FK_Patients_Lookups_AddressId",
                table: "Patients",
                column: "AddressId",
                principalTable: "Lookups",
                principalColumn: "Id");
        }
    }
}
