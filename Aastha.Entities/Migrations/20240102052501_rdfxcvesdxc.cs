﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Aastha.Entities.Migrations
{
    /// <inheritdoc />
    public partial class rdfxcvesdxc : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DiagnosisProcedure",
                table: "FormFDetails",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DiagnosisResult",
                table: "FormFDetails",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Place",
                table: "FormFDetails",
                type: "nvarchar(max)",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DiagnosisProcedure",
                table: "FormFDetails");

            migrationBuilder.DropColumn(
                name: "DiagnosisResult",
                table: "FormFDetails");

            migrationBuilder.DropColumn(
                name: "Place",
                table: "FormFDetails");
        }
    }
}
