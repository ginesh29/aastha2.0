﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Aastha.Entities.Migrations
{
    /// <inheritdoc />
    public partial class edsewsa : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "RelativeAddress",
                table: "FormFDetails",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RelativeAge",
                table: "FormFDetails",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RelativeGender",
                table: "FormFDetails",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "RelativeMobile",
                table: "FormFDetails",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RelativeName",
                table: "FormFDetails",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RelativeRelation",
                table: "FormFDetails",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ThumbImpression",
                table: "FormFDetails",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RelativeAddress",
                table: "FormFDetails");

            migrationBuilder.DropColumn(
                name: "RelativeAge",
                table: "FormFDetails");

            migrationBuilder.DropColumn(
                name: "RelativeGender",
                table: "FormFDetails");

            migrationBuilder.DropColumn(
                name: "RelativeMobile",
                table: "FormFDetails");

            migrationBuilder.DropColumn(
                name: "RelativeName",
                table: "FormFDetails");

            migrationBuilder.DropColumn(
                name: "RelativeRelation",
                table: "FormFDetails");

            migrationBuilder.DropColumn(
                name: "ThumbImpression",
                table: "FormFDetails");
        }
    }
}
