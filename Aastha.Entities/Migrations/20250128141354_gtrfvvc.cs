﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Aastha.Entities.Migrations
{
    /// <inheritdoc />
    public partial class gtrfvvc : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Payments_Ipds_IpdId",
                table: "Payments");

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_Ipds_IpdId",
                table: "Payments",
                column: "IpdId",
                principalTable: "Ipds",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Payments_Ipds_IpdId",
                table: "Payments");

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_Ipds_IpdId",
                table: "Payments",
                column: "IpdId",
                principalTable: "Ipds",
                principalColumn: "Id");
        }
    }
}
