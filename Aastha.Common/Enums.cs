﻿using System.ComponentModel.DataAnnotations;

namespace Aastha.Common
{
    public enum SortOrderEnum
    {
        Asc,
        Desc
    }
    public enum OperatorEnum
    {
        [Display(Name = "=", Description = "eq")]
        Equals,
        [Display(Name = "!=", Description = "neq")]
        NoEquals,
        [Display(Name = ">", Description = "gt")]
        GreaterThan,
        [Display(Name = "<", Description = "lt")]
        LessThan,
        [Display(Name = ">=", Description = "gte")]
        GreaterThanOrEqual,
        [Display(Name = "<=", Description = "lte")]
        LessThanOrEqual,
        [Display(Name = "Contains", Description = "contains")]
        Contains,
        [Display(Name = "StartWith", Description = "startwith")]
        StartsWith,
        [Display(Name = "EndWith", Description = "endwith")]
        EndsWith
    }
    public enum CaseTypeEnum
    {
        Old = 1,
        New = 2
    }
    public enum AppointmentTypeEnum
    {
        Date = 1,
        Sonography = 2,
        Anomaly = 3,
        Ovulation = 4,
        Cycle = 5
    }
    public enum LookupTypeEnum
    {
        DeliveryType = 1,
        OperationType = 2,
        OperationDiagnosis = 3,
        GeneralDiagnosis = 4,
        MedicinType = 5,
        Medicine = 6,
        ChargeType = 7,
        DeliveryDiagnosis = 8,
        City = 9,
        Advise = 10,
        Dist = 11,
        Taluka = 12
    }
    public enum RoomTypeEnum
    {
        General = 1,
        Special = 2,
        SemiSpecial = 3
    }
    public enum IpdTypeEnum
    {
        Delivery = 1,
        Operation = 2,
        General = 3
    }
    public enum ChildGenderEnum
    {
        Boy = 1,
        Girl = 2
    }
    public enum GenderEnum
    {
        Male = 1,
        Female = 2,
    }
    public enum RoleEnum
    {
        Admin = 1,
        Assistant = 2
    }
    public enum PaymentModeEnum
    {
        Cash = 1,
        [Display(Name = "Non-cash")]
        NonCash = 2
    }
    public enum DeptTypeEnum
    {
        Opd = 1,
        Ipd = 2
    }
}
