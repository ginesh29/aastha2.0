﻿using Aastha.Common;
using Aastha.Common.Helpers;
using Aastha.Entities.Models;
using Aastha.Repositories.Interfaces;
using Aastha.Services.DTO;
using AutoMapper;

namespace Aastha.Services
{
    public class PrescriptionService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public PrescriptionService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public PaginationModel GetPrescriptions(FilterModel filterModel)
        {
            var Prescriptions = _unitOfWork.Prescriptions.Find(null, filterModel.filter, filterModel.includeProperties, filterModel.sort);
            var totalCount = Prescriptions.Count();
            var paged = Prescriptions.ToPageList(filterModel.skip, filterModel.take);
            var mapped = _mapper.Map<List<PrescriptionDTO>>(paged).AsQueryable();
            return new PaginationModel
            {
                Data = mapped,
                StartPage = totalCount > 0 ? filterModel.skip + 1 : 0,
                EndPage = totalCount > filterModel.take ? Math.Min(filterModel.skip + filterModel.take, totalCount) : totalCount,
                TotalCount = Prescriptions.Count()
            };
        }
        public bool IsPrescriptionExist(string filter = "")
        {
            return _unitOfWork.Prescriptions.FirstOrDefault(null, filter) != null;
        }
        public PrescriptionDTO GetPrescription(long id, string filter = "", string includeProperties = "")
        {
            var Prescription = _unitOfWork.Prescriptions.FirstOrDefault(m => m.Id == id, filter, includeProperties);
            return _mapper.Map<PrescriptionDTO>(Prescription);
        }

        public void PostPrescription(PrescriptionDTO PrescriptionDto)
        {
            var prescription = _mapper.Map<Prescription>(PrescriptionDto);
            _unitOfWork.Prescriptions.Create(prescription);
            _unitOfWork.SaveChanges();
            PrescriptionDto.Id = prescription.Id;
        }
        public void PutPrescription(PrescriptionDTO PrescriptionDto)
        {
            _unitOfWork.PrescriptionMedicines.DeletePrescriptionMedicines(PrescriptionDto.Id);
            var Prescription = _mapper.Map<PrescriptionDTO, Prescription>(PrescriptionDto);
            _unitOfWork.Prescriptions.Update(Prescription);
            _unitOfWork.SaveChanges();
        }
        public void RemovePrescription(PrescriptionDTO PrescriptionDto, string filter = "", bool removePhysical = false)
        {
            var Prescription = _mapper.Map<Prescription>(PrescriptionDto);
            _unitOfWork.Prescriptions.Delete(Prescription, removePhysical);
            _unitOfWork.SaveChanges();
        }
    }
}
