﻿using Aastha.Repositories.Interfaces;
using AutoMapper;
using Microsoft.Extensions.Configuration;

namespace Aastha.Services
{
    public class ServicesWrapper
    {
        private IUnitOfWork _IUnitOfWork;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        public ServicesWrapper(IUnitOfWork IUnitOfWork, IMapper mapper, IConfiguration configuration)
        {
            _IUnitOfWork = IUnitOfWork;
            _mapper = mapper;
            _configuration = configuration;
            UserService = new UserService(_IUnitOfWork, _mapper);
            PatientService = new PatientService(_IUnitOfWork, _mapper);
            OpdService = new OpdService(_IUnitOfWork, _mapper);
            IpdService = new IpdService(_IUnitOfWork, _mapper);
            LookupService = new LookupService(_IUnitOfWork, _mapper);
            AppointmentService = new AppointmentService(_IUnitOfWork, _mapper);
            PrescriptionService = new PrescriptionService(_IUnitOfWork, _mapper);
            FormFService = new FormFService(_IUnitOfWork, _mapper, _configuration);
            PaymentService = new PaymentService(_IUnitOfWork, _mapper);
        }
        public UserService UserService;
        public PatientService PatientService;
        public OpdService OpdService;
        public IpdService IpdService;
        public LookupService LookupService;
        public AppointmentService AppointmentService;
        public PrescriptionService PrescriptionService;
        public FormFService FormFService;
        public PaymentService PaymentService;
    }
}
