﻿using Aastha.Common;
using Aastha.Common.Helpers;
using Aastha.Entities.Models;
using Aastha.Repositories.Interfaces;
using Aastha.Services.DTO;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using System.Linq.Dynamic.Core;

namespace Aastha.Services
{
    public class FormFService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        public FormFService(IUnitOfWork unitOfWork, IMapper mapper, IConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _configuration = configuration;
        }
        public PaginationModel GetFormFDetails(FilterModel filterModel)
        {
            var FormFDetails = _unitOfWork.FormFDetails.Find(null, filterModel.filter, filterModel.includeProperties, filterModel.sort);
            var totalCount = FormFDetails.Count();
            var paged = FormFDetails.ToPageList(filterModel.skip, filterModel.take);
            var mapped = _mapper.Map<List<FormFDetailDTO>>(paged).AsQueryable();
            foreach (var formF in mapped)
            {
                formF.DoctorDetail = new DoctorDetailDTO
                {
                    Name = _configuration["DoctorDetail:Name"],
                    Address1 = _configuration["DoctorDetail:AddressLine1"],
                    Address2 = _configuration["DoctorDetail:AddressLine2"],
                    District = _configuration["DoctorDetail:District"],
                    Taluka = _configuration["DoctorDetail:Taluka"],
                    Place = _configuration["DoctorDetail:Place"],
                    Pincode = _configuration["DoctorDetail:Pincode"],
                    HospitalName = _configuration["DoctorDetail:HospitalName"]
                };
            }
            return new PaginationModel
            {
                Data = mapped,
                StartPage = totalCount > 0 ? filterModel.skip + 1 : 0,
                EndPage = totalCount > filterModel.take ? Math.Min(filterModel.skip + filterModel.take, totalCount) : totalCount,
                TotalCount = FormFDetails.Count()
            };
        }
        public bool IsFormFDetailExist(string filter = "")
        {
            return _unitOfWork.FormFDetails.FirstOrDefault(null, filter) != null;
        }
        public FormFDetailDTO GetFormFDetail(long id, string filter = "", string includeProperties = "")
        {
            var FormFDetail = _unitOfWork.FormFDetails.FirstOrDefault(m => m.Id == id, filter, includeProperties);
            var result = _mapper.Map<FormFDetailDTO>(FormFDetail);
            result.DoctorDetail = new DoctorDetailDTO
            {
                Name = _configuration["DoctorDetail:Name"],
                Address1 = _configuration["DoctorDetail:AddressLine1"],
                Address2 = _configuration["DoctorDetail:AddressLine2"],
                District = _configuration["DoctorDetail:District"],
                Taluka = _configuration["DoctorDetail:Taluka"],
                Place = _configuration["DoctorDetail:Place"],
                Pincode = _configuration["DoctorDetail:Pincode"],
                HospitalName = _configuration["DoctorDetail:HospitalName"]
            };
            return result;
        }
        public void PostFormFDetail(FormFDetailDTO FormFDetailDto)
        {
            var FormFDetail = _mapper.Map<FormFDetail>(FormFDetailDto);
            _unitOfWork.FormFDetails.Create(FormFDetail);
            _unitOfWork.SaveChanges();
            FormFDetailDto.Id = FormFDetail.Id;
        }
        public void PutFormFDetail(FormFDetailDTO FormFDetailDto)
        {
            var FormFDetail = _mapper.Map<FormFDetailDTO, FormFDetail>(FormFDetailDto);
            _unitOfWork.FormFDetails.Update(FormFDetail);
            _unitOfWork.SaveChanges();
        }
        public void RemoveFormFDetail(FormFDetailDTO FormFDetailDto, string filter = "", bool removePhysical = false)
        {
            var FormFDetail = _mapper.Map<FormFDetail>(FormFDetailDto);
            _unitOfWork.FormFDetails.Delete(FormFDetail, removePhysical);
            _unitOfWork.SaveChanges();
        }
    }
}
