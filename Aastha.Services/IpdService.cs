﻿using Aastha.Common;
using Aastha.Common.Helpers;
using Aastha.Entities.Models;
using Aastha.Repositories.Interfaces;
using Aastha.Services.DTO;
using AutoMapper;
using System.Linq.Dynamic.Core;

namespace Aastha.Services
{
    public class IpdService(IUnitOfWork unitOfWork, IMapper mapper)
    {
        public PaginationModel GetIpds(FilterModel filterModel)
        {
            var ipds = unitOfWork.Ipds.Find(null, filterModel.filter, filterModel.includeProperties, filterModel.sort);
            var totalCount = ipds.Count();
            var paged = ipds.ToPageList(filterModel.skip, filterModel.take);
            var mapped = mapper.Map<List<IpdDTO>>(paged).AsQueryable();
            return new PaginationModel
            {
                Data = mapped,
                StartPage = totalCount > 0 ? filterModel.skip + 1 : 0,
                EndPage = totalCount > filterModel.take ? Math.Min(filterModel.skip + filterModel.take, totalCount) : totalCount,
                TotalCount = ipds.Count()
            };
        }
        public bool IsIpdExist(string filter = "")
        {
            return unitOfWork.Ipds.FirstOrDefault(null, filter) != null;
        }
        public IpdDTO GetIpd(long ?id, string filter = "", string includeProperties = "")
        {
            var Ipd = unitOfWork.Ipds.FirstOrDefault(m => m.Id == id, filter, includeProperties);
            return mapper.Map<IpdDTO>(Ipd);
        }
        public IQueryable GetIpdStatistics(int? Year = null)
        {
            return unitOfWork.Ipds.GetStatistics(Year).AsQueryable();
        }
        public void PostIpd(IpdDTO IpdDto)
        {
            var Ipd = mapper.Map<Ipd>(IpdDto);
            unitOfWork.Ipds.Create(Ipd);
            unitOfWork.SaveChanges();
            if (IpdDto.Type == IpdTypeEnum.Delivery)
            {
                var delivery = mapper.Map<Delivery>(IpdDto.DeliveryDetail);
                delivery.IpdId = Ipd.Id;
                unitOfWork.Deliveries.Create(delivery);
            }
            if (IpdDto.Type == IpdTypeEnum.Operation)
            {
                var operation = mapper.Map<Operation>(IpdDto.OperationDetail);
                operation.IpdId = Ipd.Id;
                unitOfWork.Operations.Create(operation);
            }
            unitOfWork.SaveChanges();
            IpdDto.Id = Ipd.Id;
        }
        public void PutIpd(IpdDTO IpdDto)
        {
            var Ipd = mapper.Map<IpdDTO, Ipd>(IpdDto);
            unitOfWork.Ipds.Update(Ipd);
            unitOfWork.SaveChanges();
        }
        public void RemoveIpd(IpdDTO IpdDto, string filter = "", bool removePhysical = false)
        {
            var Ipd = mapper.Map<Ipd>(IpdDto);
            unitOfWork.Ipds.Delete(Ipd, removePhysical);
            unitOfWork.SaveChanges();
        }
        public void RemoveIpdLookup(IEnumerable<IpdLookupDTO> ipdLookupDTO, string filter = "", bool removePhysical = false)
        {
            foreach (var item in ipdLookupDTO)
            {
                var Ipd = unitOfWork.IpdLookups.FirstOrDefault(m => m.Id == item.Id, filter);
                unitOfWork.IpdLookups.Delete(Ipd, removePhysical);
            }
            unitOfWork.SaveChanges();
        }
    }
}
