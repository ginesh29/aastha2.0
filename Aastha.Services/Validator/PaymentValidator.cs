﻿using Aastha.Common;
using Aastha.Common.Helpers;
using Aastha.Services.DTO;
using FluentValidation;

namespace Aastha.Services.Validator
{
    public class PaymentValidator : AbstractValidator<PaymentDTO>
    {
        private readonly OpdService _opdService;
        private readonly IpdService _ipdService;
        public PaymentValidator(ServicesWrapper ServicesWrapper)
        {
            _ipdService = ServicesWrapper.IpdService;
            _opdService = ServicesWrapper.OpdService;
            RuleFor(m => m.PaymentDate).NotEmpty().When(m => m.Id < 1).WithMessage("Date is required");
            RuleFor(m => m.PaymentMode).NotEmpty().When(m => m.Id < 1).WithMessage("Mode is required");
            RuleFor(m => m.Amount).NotEmpty().When(m => m.Id < 1).WithMessage("Amount is required");
            RuleFor(x => x).Must(PaymentAmountExceed).WithMessage("Payment amount exceed.");
        }

        public bool PaymentAmountExceed(PaymentDTO payment)
        {
            if (payment.OpdId > 0)
            {
                var opd = _opdService.GetOpd(payment.OpdId, $"isDeleted-neq-{{true}}", "Payments");
                var paidAmount = opd.Payments.Where(m => m.Dept == DeptTypeEnum.Opd && m.Id != payment.Id).Sum(x => x.Amount);
                var remainingAmount = opd.TotalCharge - paidAmount;
                if (remainingAmount < payment.Amount)
                    return false;
            }
            else if (payment.IpdId > 0)
            {
                var ipd = _ipdService.GetIpd(payment.IpdId, $"isDeleted-neq-{{true}}", "Charges,Payments");
                var totalAmount = ipd.Charges.Sum(x => x.Amount);
                var paidAmount = ipd.Payments.Where(m => m.Dept == DeptTypeEnum.Ipd && m.Id != payment.Id).Sum(x => x.Amount);
                var remainingAmount = totalAmount - ipd.Discount - paidAmount;
                if (remainingAmount < payment.Amount)
                    return false;
                return true;
            }
            return true;
        }
    }
}
