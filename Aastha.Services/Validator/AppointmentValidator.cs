﻿using Aastha.Services.DTO;
using FluentValidation;

namespace Aastha.Services.Validator
{
    public class AppointmentValidator : AbstractValidator<AppointmentDTO>
    {
        private readonly AppointmentService _appointmentService;
        private readonly PatientService _patientService;
        public AppointmentValidator(ServicesWrapper ServicesWrapper)
        {
            _appointmentService = ServicesWrapper.AppointmentService;
            _patientService = ServicesWrapper.PatientService;
            RuleFor(m => m.Date).NotEmpty().When(m => m.Id < 1);
            RuleFor(m => m.Type).NotEmpty().When(m => m.Id < 1)
                                .IsInEnum();
            RuleFor(m => m.PatientId).NotNull().When(m => m.Id < 1);
            RuleFor(x => x).Must(IsExistAppointment).WithMessage("Appointment already exist.");
            RuleFor(x => x).Must(IsValidPatient).WithMessage("Select valid Patient.");
        }
        public bool IsValidPatient(AppointmentDTO appointment)
        {
            long patientId = Convert.ToInt64(appointment.PatientId);
            string filter = $"Id-eq-{{{patientId}}}";
            if (appointment != null && !_patientService.IsPatientExist(filter))
                return false;
            return true;
        }
        private bool IsExistAppointment(AppointmentDTO appointment)
        {
            string filter = $"id-neq-{{{appointment.Id}}} and date-eq-{{{appointment.Date.ToString("MM-dd-yyyy")}}} and patientId-eq-{{{appointment.PatientId}}} and isDeleted-neq-{{{true}}}";
            if (appointment != null && _appointmentService.IsAppointmentExist(filter))
                return false;
            return true;
        }
    }
}