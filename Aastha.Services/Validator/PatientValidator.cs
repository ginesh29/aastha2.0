﻿using Aastha.Services.DTO;
using FluentValidation;

namespace Aastha.Services.Validator
{
    public class PatientValidator : AbstractValidator<PatientDTO>
    {
        private readonly PatientService _patientService;
        private readonly LookupService _lookupService;
        public PatientValidator(ServicesWrapper ServicesWrapper)
        {
            _patientService = ServicesWrapper.PatientService;
            _lookupService = ServicesWrapper.LookupService;
            RuleFor(m => m.Firstname).NotEmpty().When(m => m.Id < 1).WithMessage("Firstname is required");
            RuleFor(m => m.Middlename).NotEmpty().When(m => m.Id < 1).WithMessage("Middlename is required");
            RuleFor(m => m.Lastname).NotEmpty().When(m => m.Id < 1).WithMessage("Lastname is required");
            RuleFor(m => m.CityId).NotEmpty().When(m => m.Id < 1).WithMessage("City/Village is required");
            RuleFor(m => m.Age).NotNull().When(m => m.Id < 1).WithMessage("Age is required")
            .GreaterThan(0).When(m => m.Id < 1).WithMessage("Age must be between 1 to 100.")
            .LessThanOrEqualTo(100).WithMessage("Age must be between 1 to 100.");
            RuleFor(x => x).Must(IsValidCity).WithMessage("City/Village not valid.");
            RuleFor(x => x).Must(IsExistPatient).WithMessage("Patient already exist.");
        }
        private bool IsValidCity(PatientDTO patient)
        {
            long lookupId = patient.CityId;
            string filter = $"Id-eq-{{{lookupId}}}";
            if (!_lookupService.IsLookupExist(filter))
                return false;
            return true;
        }
        private bool IsExistPatient(PatientDTO patient)
        {
            string filter = $"id-neq-{{{patient.Id}}} and Firstname-eq-{{{patient.Firstname}}} and Middlename-eq-{{{patient.Middlename}}} and Lastname-eq-{{{patient.Lastname}}} and isDeleted-neq-{{{true}}}";
            if (!string.IsNullOrEmpty(patient.Fathername))
                filter = $"{filter} and Fathername-eq-{{{patient.Fathername}}}";
            if (patient != null && _patientService.IsPatientExist(filter))
                return false;
            return true;
        }
    }
}