﻿using Aastha.Services.DTO;
using FluentValidation;

namespace Aastha.Services.Validator
{
    public class UserValidator : AbstractValidator<UserDTO>
    {
        public UserValidator()
        {
            RuleFor(m => m.Firstname).NotEmpty().When(m => m.Id < 1).WithMessage("Firstname is required");
            RuleFor(m => m.Middlename).NotEmpty().When(m => m.Id < 1).WithMessage("Middlename is required");
            RuleFor(m => m.Lastname).NotEmpty().When(m => m.Id < 1).WithMessage("Lastname is required");
            RuleFor(m => m.Username).NotEmpty().When(m => m.Id < 1).WithMessage("Username is required");
        }
    }
}