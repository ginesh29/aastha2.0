﻿using Aastha.Services.DTO;
using FluentValidation;

namespace Aastha.Services.Validator
{
    public class OpdValidator : AbstractValidator<OpdDTO>
    {
        private readonly OpdService _opdService;
        private readonly PatientService _patientService;
        public OpdValidator(ServicesWrapper ServicesWrapper)
        {
            _opdService = ServicesWrapper.OpdService;
            _patientService = ServicesWrapper.PatientService;
            RuleFor(m => m.CaseType).NotEmpty().When(m => m.Id < 1).WithMessage("Case Type is required").IsInEnum();
            RuleFor(m => m.Date).NotEmpty().When(m => m.Id < 1).WithMessage("Opd Date is required");
            RuleFor(m => m.PatientId).NotNull().When(m => m.Id < 1).WithMessage("Select Patient");
            RuleFor(x => x).Must(IsValidPatient).WithMessage("Select valid Patient.");
            RuleFor(x => x).Must(IsExistOpd).WithMessage("Patient already exist.");
        }
        public bool IsValidPatient(OpdDTO opd)
        {
            long patientId = Convert.ToInt64(opd.PatientId);
            string filter = $"Id-eq-{{{patientId}}}";
            if (opd != null && !_patientService.IsPatientExist(filter))
                return false;
            return true;
        }
        private bool IsExistOpd(OpdDTO opd)
        {
            string filter = $"Id-neq-{{{opd.Id}}} and date-eq-{{{Convert.ToDateTime(opd.Date).ToString("MM-dd-yyyy")}}} and patientId-eq-{{{opd.PatientId}}} and isDeleted-neq-{{{true}}}";
            if (opd != null && _opdService.IsOpdExist(filter) && opd.CheckExist)
                return false;
            return true;
        }        
    }
}