﻿using Aastha.Services.DTO;
using FluentValidation;

namespace Aastha.Services.Validator
{
    public class LookupValidator : AbstractValidator<LookupDTO>
    {
        private readonly LookupService _lookupService;
        public LookupValidator(ServicesWrapper ServicesWrapper)
        {
            _lookupService = ServicesWrapper.LookupService;
            RuleFor(m => m.Name).NotEmpty().When(m => m.Id < 1).WithMessage("name is required");
            RuleFor(m => m.Type).NotEmpty().When(m => m.Id < 1).WithMessage("type is required");
            RuleFor(m => new { m.Id, m.Type, m.Name });
            RuleFor(x => x).Must(IsExistLookup).WithMessage("Lookup already exist.");
        }
        private bool IsExistLookup(LookupDTO lookup)
        {
            string filter = $"Id-neq-{{{lookup.Id}}} and parentId-eq-{{{lookup.ParentId}}} and type-eq-{{{lookup.Type}}} and name-eq-{{{lookup.Name}}} and isDeleted-neq-{{{true}}}";
            if (lookup != null && _lookupService.IsLookupExist(filter))
                return false;
            return true;
        }
    }
}