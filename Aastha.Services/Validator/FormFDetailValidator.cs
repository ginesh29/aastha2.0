﻿using Aastha.Services.DTO;
using FluentValidation;

namespace Aastha.Services.Validator
{
    public class FormFDetailValidator : AbstractValidator<FormFDetailDTO>
    {
        private readonly PatientService _patientService;
        public FormFDetailValidator(ServicesWrapper ServicesWrapper)
        {
            _patientService = ServicesWrapper.PatientService;
            RuleFor(m => m.Date).NotEmpty().When(m => m.Id < 1).WithMessage("Ultrasound Date is required");
            RuleFor(m => m.LMPDate).NotEmpty().When(m => m.Id < 1).WithMessage("LMP Date is required");
            RuleFor(m => m.PatientId).NotNull().When(m => m.Id < 1).WithMessage("Select Patient");
            RuleFor(m => m.DiagnosisProcedure).NotEmpty().When(m => m.Id < 1).WithMessage("Diagnosis Procedure is required");
            RuleFor(m => m.DiagnosisResult).NotEmpty().When(m => m.Id < 1).WithMessage("Diagnosis Result is required");
            RuleFor(x => x).Must(IsValidPatient).WithMessage("Select valid Patient.");
            RuleFor(x => x).Must(IsPatientDetailComplete).WithMessage("Patient Address,Dist,Taluka & Age not filled.");
        }
        public bool IsValidPatient(FormFDetailDTO formFDetail)
        {
            long patientId = Convert.ToInt64(formFDetail.PatientId);
            string filter = $"Id-eq-{{{patientId}}}";
            if (formFDetail != null && !_patientService.IsPatientExist(filter))
                return false;
            return true;
        }
        private bool IsPatientDetailComplete(FormFDetailDTO formFDetail)
        {
            var patient = _patientService.GetPatient(formFDetail.PatientId);
            if (patient.DistId == null || patient.TalukaId == null || patient.CalculatedAge <= 0 || string.IsNullOrEmpty(patient.Address1))
                return false;
            return true;
        }
    }
}