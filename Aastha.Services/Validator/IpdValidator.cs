﻿using Aastha.Common;
using Aastha.Common.Helpers;
using Aastha.Services.DTO;
using FluentValidation;

namespace Aastha.Services.Validator
{
    public class IpdValidator : AbstractValidator<IpdDTO>
    {
        private readonly PatientService _patientService;
        private readonly LookupService _lookupService;
        private readonly IpdService _ipdService;
        public IpdValidator(ServicesWrapper ServicesWrapper)
        {
            _patientService = ServicesWrapper.PatientService;
            _lookupService = ServicesWrapper.LookupService;
            _ipdService = ServicesWrapper.IpdService;
            RuleFor(m => m.Type).NotEmpty().When(m => m.Id < 1).WithMessage("Ipd Type is required")
                                .IsInEnum();
            RuleFor(m => m.RoomType).NotEmpty().When(m => m.Id < 1).WithMessage("Room Type is required")
                                    .IsInEnum();
            RuleFor(m => m.PatientId).NotEmpty().When(m => m.Id < 1).WithMessage("Select Patient");

            RuleFor(m => new { m.Id, m.UniqueId }).NotEmpty().When(m => m.Id < 1).WithMessage("Select Invoice No.");

            RuleFor(m => m.AddmissionDate).NotEmpty().When(m => m.Id < 1).WithMessage("Addmission Date is required");
            RuleFor(m => m.OperationDetail).NotNull().When(m => m.Id < 1 && m.Type == IpdTypeEnum.Operation)
            .SetValidator(new OperationDetailValidator()).When(m => m.Id < 1 && m.Type == IpdTypeEnum.Operation);

            RuleFor(m => m.DeliveryDetail).NotNull().When(m => m.Id < 1 && m.Type == IpdTypeEnum.Delivery)
            .SetValidator(new DeliveryDetailValidator()).When(m => m.Id < 1 && m.Type == IpdTypeEnum.Delivery);

            RuleFor(m => m.IpdLookups).NotEmpty().When(m => m.Id < 1);
            RuleFor(m => m.Charges).NotEmpty().When(m => m.Id < 1).WithMessage("Please enter charges detail.");

            RuleForEach(x => x.Charges).Must(IsValidCharge).WithMessage("Charges Details not valid.");
            RuleForEach(x => x.IpdLookups).Must(IsValidLookup).WithMessage("Lookups Details not valid.");
            RuleFor(x => x).Must(IsValidPatient).WithMessage("Select valid Patient.");
            RuleFor(x => x).Must(IsExistUniqueId).WithMessage("Invoice No. already exist.");
        }
        public bool IsValidPatient(IpdDTO ipd)
        {
            long patientId = Convert.ToInt64(ipd.PatientId);
            string filter = $"Id-eq-{{{patientId}}}";
            if (ipd != null && !_patientService.IsPatientExist(filter))
                return false;
            return true;
        }
        private bool IsValidCharge(ChargeDTO charge)
        {
            string filter = $"Id-eq-{{{charge.LookupId}}}";
            if (!_lookupService.IsLookupExist(filter))
                return false;
            return true;
        }
        private bool IsValidLookup(IpdLookupDTO ipdLookup)
        {
            string filter = $"Id-eq-{{{ipdLookup.LookupId}}}";
            if (!_lookupService.IsLookupExist(filter))
                return false;
            return true;
        }
        public bool IsExistUniqueId(IpdDTO ipd)
        {
            string filter = $"Id-neq-{{{ipd.Id}}} and UniqueId-eq-{{{ipd.UniqueId}}} and isDeleted-neq-{{{true}}}";
            if (ipd != null && _ipdService.IsIpdExist(filter))
                return false;
            return true;
        }
    }
    public class OperationDetailValidator : AbstractValidator<OperationDTO>
    {
        public OperationDetailValidator()
        {
            RuleFor(x => x.Date).NotEmpty();
        }
    }
    public class DeliveryDetailValidator : AbstractValidator<DeliveryDTO>
    {
        public DeliveryDetailValidator()
        {
            RuleFor(x => x.Date).NotEmpty();
            RuleFor(x => x.Time).NotEmpty()
                                .Must(ValidateHelper.IsValidTime).WithMessage("Invalid Delivery Time");
            RuleFor(x => x.Gender).NotEmpty();
            RuleFor(x => x.BabyWeight).NotEmpty();
        }


    }

}
