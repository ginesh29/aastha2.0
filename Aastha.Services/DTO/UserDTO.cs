﻿namespace Aastha.Services.DTO
{
    public class UserDTO
    {
        public long Id { get; set; }
        public string Firstname { get; set; }
        public string Middlename { get; set; }
        public string Lastname { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsSuperAdmin { get; set; }
        public string FormFUsername { get; set; }
        public string FormFPassword { get; set; }
        public string Fullname => $"{Firstname} {Middlename} {Lastname}";
        public bool IsDeleted { get; set; }
    }
}
