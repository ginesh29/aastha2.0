﻿using Aastha.Common;

namespace Aastha.Services.DTO
{
    public class LookupDTO
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public LookupTypeEnum Type { get; set; }
        public string TypeName => Type.GetDisplayName();
        public long? ParentId { get; set; }
        public bool? IsDeleted { get; set; }

        public LookupDTO Parent { get; set; }
        public ICollection<LookupDTO> Children { get; set; }
    }
}
