﻿
namespace Aastha.Services.DTO
{
    public class PatientDTO
    {
        public long Id { get; set; }
        public string Firstname { get; set; }
        public string Middlename { get; set; }
        public string Fathername { get; set; }
        public string Lastname { get; set; }
        public string Fullname => $"{Firstname} {Middlename}{(!string.IsNullOrEmpty(Fathername) ? $"({Fathername})" : string.Empty)} {Lastname}";
        public string Name => $"{Firstname} {Middlename} {Lastname}";
        public string Mobile { get; set; }
        public int Age { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime ModifiedDate { get; set; }
        public long CityId { get; set; }
        public LookupDTO City { get; set; }
        public long? DistId { get; set; }
        public LookupDTO Dist { get; set; }
        public long? TalukaId { get; set; }
        public LookupDTO Taluka { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public DateTime? BirthDate { get; set; }
        public int CalculatedAge
        {
            get
            {
                if (BirthDate != null)
                {
                    DateTime today = DateTime.Today;
                    var birthDate = Convert.ToDateTime(BirthDate);
                    int age = today.Year - birthDate.Year;
                    if (birthDate.Date > today.AddYears(-age))
                        age--;
                    return age;
                }
                else
                    return Age;
            }
        }
    }
}
