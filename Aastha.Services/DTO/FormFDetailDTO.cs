﻿using Aastha.Common;

namespace Aastha.Services.DTO
{
    public class FormFDetailDTO
    {
        public long Id { get; set; }
        public DateTime Date { get; set; }
        public DateTime LMPDate { get; set; }
        public long PatientId { get; set; }
        public PatientDTO Patient { get; set; }
        public int[] DiagnosisProcedure { get; set; }
        public string DiagnosisResult { get; set; }
        public List<ChildDTO> Children { get; set; }
        public DoctorDetailDTO DoctorDetail { get; set; }
        public bool ThumbImpression { get; set; }
        public GenderEnum RelativeGender { get; set; }
        public string RelativeGenderName => RelativeGender.ToString();
        public string RelativeRelation { get; set; }
        public string RelativeName { get; set; }
        public int RelativeAge { get; set; }
        public string RelativeAddress { get; set; }
        public string RelativeMobile { get; set; }
        public bool IsDeleted { get; set; }
        public bool Submitted { get; set; }
    }
    public class ChildDTO
    {
        public int No { get; set; }
        public int Type { get; set; }
        public int AgeYr { get; set; }
        public int AgeMn { get; set; }
    }
    public class DoctorDetailDTO
    {
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string District { get; set; }
        public string Taluka { get; set; }
        public string Place { get; set; }
        public string Pincode { get; set; }
        public string HospitalName { get; set; }
    }
}
