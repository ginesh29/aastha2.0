﻿using Aastha.Entities.Models;

namespace Aastha.Services.DTO
{
    public class PrescriptionMedicineDTO
    {
        public int Days { get; set; }
        public int Qty { get; set; }
        public string MedicineType { get; set; }
        public string MedicineName { get; set; }
        public string MedicineInstruction { get; set; }
        public long PrescriptionId { get; set; }
    }
}
