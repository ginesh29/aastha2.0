﻿using Aastha.Common;
using Aastha.Entities.Models;

namespace Aastha.Services.DTO
{
    public class PaymentDTO
    {
        public long Id { get; set; }
        public long? IpdId { get; set; }
        public long? OpdId { get; set; }
        public DateTime PaymentDate { get; set; }
        public PaymentModeEnum PaymentMode { get; set; }
        public string PaymentModeName => PaymentMode.GetDisplayName();
        public decimal Amount { get; set; }
        public DeptTypeEnum Dept { get; set; }
        public bool IsDeleted { get; set; }
    }
}