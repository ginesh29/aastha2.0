﻿using Aastha.Common;

namespace Aastha.Services.DTO
{
    public class AppointmentDTO
    {
        public long Id { get; set; }
        public DateTime Date { get; set; }
        public AppointmentTypeEnum? Type { get; set; }
        public string AppointmentType => Type.GetDisplayName();
        public long? PatientId { get; set; }
        public bool? IsDeleted { get; set; }
        public PatientDTO Patient { get; set; }
    }
}
