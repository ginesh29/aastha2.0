﻿using Aastha.Common;

namespace Aastha.Services.DTO
{
    public class DeliveryDTO
    {
        public long Id { get; set; }
        public long IpdId { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan Time { get; set; }
        public ChildGenderEnum? Gender { get; set; }
        public string GenderName => Gender.GetDisplayName();
        public DateTime DateTime => Date + Time;
        public decimal BabyWeight { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
