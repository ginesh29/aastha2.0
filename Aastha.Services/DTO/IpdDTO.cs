﻿using Aastha.Common;

namespace Aastha.Services.DTO
{
    public class IpdDTO
    {
        public long Id { get; set; }
        public long UniqueId { get; set; }
        public string InvoiceNo => $"IPD{UniqueId.ToString().PadLeft(7, '0')}";
        public IpdTypeEnum? Type { get; set; }
        public string IpdType => Type.GetDisplayName();
        public RoomTypeEnum? RoomType { get; set; }
        public string RoomTypeName => RoomType.GetDisplayName();
        public DateTime AddmissionDate { get; set; }
        public DateTime? DischargeDate { get; set; }
        public decimal Discount { get; set; }
        public bool? IsDeleted { get; set; }

        public long? PatientId { get; set; }
        public PatientDTO Patient { get; set; }
        public DeliveryDTO DeliveryDetail { get; set; }
        public OperationDTO OperationDetail { get; set; }
        public ICollection<ChargeDTO> Charges { get; set; }
        public ICollection<IpdLookupDTO> IpdLookups { get; set; }
        public ICollection<PaymentDTO> Payments { get; set; }
    }
}
