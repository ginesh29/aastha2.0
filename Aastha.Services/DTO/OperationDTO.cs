﻿namespace Aastha.Services.DTO
{
    public class OperationDTO
    {
        public long Id { get; set; }
        public long IpdId { get; set; }
        public DateTime Date { get; set; }
    }
}
