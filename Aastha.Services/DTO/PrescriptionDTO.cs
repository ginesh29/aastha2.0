﻿using Aastha.Entities.Models;

namespace Aastha.Services.DTO
{
    public class PrescriptionDTO
    {
        public long Id { get; set; }
        public DateTime Date { get; set; }
        public long PatientId { get; set; }
        public Patient Patient { get; set; }
        public string ClinicalDetail { get; set; }
        public int FollowupType { get; set; }
        public DateTime? FollowupDate { get; set; }
        public string Advices { get; set; }
        public ICollection<PrescriptionMedicineDTO> PrescriptionMedicines { get; set; }
        public bool IsDeleted { get; set; }
    }
}
