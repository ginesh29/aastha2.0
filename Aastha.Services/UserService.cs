﻿using Aastha.Common;
using Aastha.Common.Helpers;
using Aastha.Entities.Models;
using Aastha.Repositories.Interfaces;
using Aastha.Services.DTO;
using AutoMapper;

namespace Aastha.Services
{
    public class UserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public PaginationModel GetUsers(FilterModel filterModel)
        {
            var users = _unitOfWork.Users.Find(null, filterModel.filter, filterModel.includeProperties, filterModel.sort);
            var totalCount = users.Count();
            var paged = users.ToPageList(filterModel.skip, filterModel.take);
            var mapped = _mapper.Map<List<UserDTO>>(paged).AsQueryable();
            return new PaginationModel
            {
                Data = mapped,
                StartPage = totalCount > 0 ? filterModel.skip + 1 : 0,
                EndPage = totalCount > filterModel.take ? Math.Min(filterModel.skip + filterModel.take, totalCount) : totalCount,
                TotalCount = users.Count()
            };
        }
        public bool IsUserExist(string filter = "")
        {
            return _unitOfWork.Users.FirstOrDefault(null, filter) != null;
        }
        public UserDTO VerifyUser(LoginModel loginModel)
        {
            var user = _unitOfWork.Users.FirstOrDefault(m => m.Username == loginModel.Username && m.Password == loginModel.Password);
            return _mapper.Map<UserDTO>(user);
        }
        public UserDTO VerifyFormF(LoginModel loginModel)
        {
            loginModel.Password = PasswordHash.GenerateHash(loginModel.Password);
            var user = _unitOfWork.Users.FirstOrDefault(m => m.FormFUsername == loginModel.Username && m.FormFPassword == loginModel.Password);
            return _mapper.Map<UserDTO>(user);
        }
        public UserDTO GetUser(long id, string filter = "", string includeProperties = "")
        {
            var user = _unitOfWork.Users.FirstOrDefault(m => m.Id == id, filter, includeProperties);
            user.Password = "";
            user.FormFPassword = "";
            return _mapper.Map<UserDTO>(user);
        }

        public void PostUser(UserDTO userDto)
        {
            userDto.Password = PasswordHash.GenerateHash(userDto.Password);
            userDto.FormFPassword = PasswordHash.GenerateHash(userDto.FormFPassword);
            var user = _mapper.Map<User>(userDto);
            _unitOfWork.Users.Create(user);
            _unitOfWork.SaveChanges();
            userDto.Id = user.Id;
        }
        public void PutUser(UserDTO userDto)
        {
            userDto.Password = PasswordHash.GenerateHash(userDto.Password);
            userDto.FormFPassword = PasswordHash.GenerateHash(userDto.FormFPassword);
            var user = _mapper.Map<UserDTO, User>(userDto);
            _unitOfWork.Users.Update(user);
            _unitOfWork.SaveChanges();
        }
        public void ChangeFormFPassword(long userId, string password)
        {
            var user = _unitOfWork.Users.FirstOrDefault(m => m.Id == userId);
            user.FormFPassword = PasswordHash.GenerateHash(password);
            _unitOfWork.Users.Update(user);
            _unitOfWork.SaveChanges();
        }
        public void RemoveUser(UserDTO userDto, string filter = "", bool removePhysical = false)
        {
            var user = _mapper.Map<User>(userDto);
            _unitOfWork.Users.Delete(user, removePhysical);
            _unitOfWork.SaveChanges();
        }
    }
}
