﻿using Aastha.Common;
using Aastha.Common.Helpers;
using Aastha.Entities.Models;
using Aastha.Repositories.Interfaces;
using Aastha.Services.DTO;
using AutoMapper;
using System.Linq.Dynamic.Core;

namespace Aastha.Services
{
    public class PaymentService(IUnitOfWork unitOfWork, IMapper mapper)
    {
        private readonly IUnitOfWork _unitOfWork = unitOfWork;
        private readonly IMapper _mapper = mapper;

        public PaginationModel GetPayments(FilterModel filterModel)
        {
            var payments = _unitOfWork.Payments.Find(null, filterModel.filter, filterModel.includeProperties, filterModel.sort);
            var totalCount = payments.Count();
            var paged = payments.ToPageList(filterModel.skip, filterModel.take);
            var mapped = _mapper.Map<List<PaymentDTO>>(paged).AsQueryable();            
            return new PaginationModel
            {
                Data = mapped,
                StartPage = totalCount > 0 ? filterModel.skip + 1 : 0,
                EndPage = totalCount > filterModel.take ? Math.Min(filterModel.skip + filterModel.take, totalCount) : totalCount,
                TotalCount = payments.Count()
            };
        }
        public bool IsPaymentExist(string filter = "")
        {
            return _unitOfWork.Payments.FirstOrDefault(null, filter) != null;
        }
        public PaymentDTO GetPayment(long id, string filter = "", string includeProperties = "")
        {
            var payment = _unitOfWork.Payments.FirstOrDefault(m => m.Id == id, filter, includeProperties);
            return _mapper.Map<PaymentDTO>(payment);
        }
        public void PostPayment(PaymentDTO PaymentDto)
        {
            var Payment = _mapper.Map<Payment>(PaymentDto);
            _unitOfWork.Payments.Create(Payment);
            _unitOfWork.SaveChanges();
            PaymentDto.Id = Payment.Id;
        }
        public void PutPayment(PaymentDTO PaymentDto)
        {
            var Payment = _mapper.Map<PaymentDTO, Payment>(PaymentDto);
            _unitOfWork.Payments.Update(Payment);
            _unitOfWork.SaveChanges();
        }
        public void RemovePayment(PaymentDTO PaymentDto, bool removePhysical = false)
        {
            var Payment = _mapper.Map<Payment>(PaymentDto);
            _unitOfWork.Payments.Delete(Payment, removePhysical);
            _unitOfWork.SaveChanges();
        }
    }
}
