﻿using Aastha.Entities.Data;
using Aastha.Entities.Models;
using Aastha.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Aastha.Repositories
{
    public class PrescriptionMedicineRepository : RepositoryBase<PrescriptionMedicine>, IPrescriptionMedicineRepository
    {
        public PrescriptionMedicineRepository(ApplicationDbContext AASTHA2Context)
            : base(AASTHA2Context)
        {
        }
        public void DeletePrescriptionMedicines(long prescriptionId)
        {
            _AASTHA2Context.Database.ExecuteSqlInterpolated($"EXEC DeletePrescriptionMedicines {prescriptionId}");
        }

    }
}
