﻿using Aastha.Entities.Data;
using Aastha.Entities.Models;
using Aastha.Repositories.Interfaces;

namespace Aastha.Repositories
{
    public class DeliveryRepository : RepositoryBase<Delivery>, IDeliveryRepository
    {
        public DeliveryRepository(ApplicationDbContext AASTHA2Context)
            : base(AASTHA2Context)
        {
        }
    }
}
