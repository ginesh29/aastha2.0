﻿using Aastha.Entities.Data;
using Aastha.Entities.Models;
using Aastha.Repositories.Interfaces;

namespace Aastha.Repositories
{
    public class FormFRepository : RepositoryBase<FormFDetail>, IFormFRepository
    {
        public FormFRepository(ApplicationDbContext AASTHA2Context)
            : base(AASTHA2Context)
        {
        }
    }
}
