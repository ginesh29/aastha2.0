﻿using Aastha.Entities.Data;
using Aastha.Entities.Models;
using Aastha.Repositories.Interfaces;

namespace Aastha.Repositories
{
    public class ChargeRepository : RepositoryBase<Charge>, IChargeRepository
    {
        public ChargeRepository(ApplicationDbContext AASTHA2Context)
            : base(AASTHA2Context)
        {
        }
    }
}
