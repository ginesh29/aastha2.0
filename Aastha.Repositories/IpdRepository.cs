﻿using Aastha.Entities.Data;
using Aastha.Entities.Models;
using Aastha.Entities.StoredProcedure;
using Aastha.Repositories.Interfaces;
using StoredProcedureEFCore;
using System.Linq.Dynamic.Core;

namespace Aastha.Repositories
{
    public class IpdRepository : RepositoryBase<Ipd>, IIpdRepository
    {
        public IpdRepository(ApplicationDbContext AASTHA2Context)
            : base(AASTHA2Context)
        {
        }
        public IEnumerable<Sp_GetStatistics_Result> GetStatistics(int? Year)
        {
            IEnumerable<Sp_GetStatistics_Result> rows = null;
            _AASTHA2Context.LoadStoredProc("GetIpdStatistics").Exec(r => rows = r.ToList<Sp_GetStatistics_Result>());
            if (Year > 0)
                rows = rows.Where(m => m.Year == Year);
            return rows;
        }
    }
}
