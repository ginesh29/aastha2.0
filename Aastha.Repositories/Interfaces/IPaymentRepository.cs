﻿using Aastha.Entities.Models;

namespace Aastha.Repositories.Interfaces
{
    public interface IPaymentRepository : IRepository<Payment>
    {
    }
}
