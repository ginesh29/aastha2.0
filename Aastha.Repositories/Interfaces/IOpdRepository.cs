﻿using Aastha.Entities.Models;
using Aastha.Entities.StoredProcedure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aastha.Repositories.Interfaces
{
    public interface IOpdRepository : IRepository<Opd>
    {
        IEnumerable<Sp_GetStatistics_Result> GetStatistics(int? Year);
    }
}
