﻿using Aastha.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aastha.Repositories.Interfaces
{
    public interface IDeliveryRepository : IRepository<Delivery>
    {
    }
}
