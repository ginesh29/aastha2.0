﻿using Aastha.Entities.Models;

namespace Aastha.Repositories.Interfaces
{
    public interface IFormFRepository : IRepository<FormFDetail>
    {
    }
}
