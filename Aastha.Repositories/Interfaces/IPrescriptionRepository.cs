﻿using Aastha.Entities.Models;

namespace Aastha.Repositories.Interfaces
{
    public interface IPrescriptionRepository : IRepository<Prescription>
    {
        
    }
}
