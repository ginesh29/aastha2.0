﻿using Aastha.Entities.Models;
using Aastha.Entities.StoredProcedure;

namespace Aastha.Repositories.Interfaces
{
    public interface IPatientRepository : IRepository<Patient>
    {
        IEnumerable<Sp_GetStatistics_Result> GetStatistics(int? Year);
        IEnumerable<Sp_GetPatientsHistories> GetPatientsHistories(DateTime? startDate, DateTime? endDate, int? month, int? year, int? patientId);
    }
}
