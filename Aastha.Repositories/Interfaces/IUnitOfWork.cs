﻿namespace Aastha.Repositories.Interfaces
{
    public interface IUnitOfWork
    {
        IUserRepository Users { get; }
        IPatientRepository Patients { get; }
        IOpdRepository Opds { get; }
        IIpdRepository Ipds { get; }
        IIpdLookupRepository IpdLookups { get; }
        ILookupRepository Lookups { get; }
        IOperationRepository Operations { get; }
        IDeliveryRepository Deliveries { get; }
        IChargeRepository Charges { get; }
        IAppointmentRepository Appointments { get; }
        IPrescriptionRepository Prescriptions { get; }
        IPrescriptionMedicineRepository PrescriptionMedicines { get; }
        IFormFRepository FormFDetails { get; }
        IPaymentRepository Payments { get; }
        void SaveChanges();
    }
}
