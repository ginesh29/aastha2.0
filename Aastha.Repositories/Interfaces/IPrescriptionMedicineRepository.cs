﻿using Aastha.Entities.Models;

namespace Aastha.Repositories.Interfaces
{
    public interface IPrescriptionMedicineRepository : IRepository<PrescriptionMedicine>
    {
       void DeletePrescriptionMedicines(long prescriptionId);
    }
}
