﻿using Aastha.Entities.Data;
using Aastha.Entities.Models;
using Aastha.Entities.StoredProcedure;
using Aastha.Repositories.Interfaces;
using StoredProcedureEFCore;

namespace Aastha.Repositories
{
    public class OpdRepository(ApplicationDbContext AASTHA2Context) : RepositoryBase<Opd>(AASTHA2Context), IOpdRepository
    {
        public IEnumerable<Sp_GetStatistics_Result> GetStatistics(int? Year)
        {
            IEnumerable<Sp_GetStatistics_Result> rows = null;
            _AASTHA2Context.LoadStoredProc("GetOpdStatistics").Exec(r => rows = r.ToList<Sp_GetStatistics_Result>());
            if (Year > 0)
                rows = rows.Where(m => m.Year == Year);
            return rows;
        }
    }
}
