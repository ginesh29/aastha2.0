﻿using Aastha.Entities.Data;
using Aastha.Repositories.Interfaces;

namespace Aastha.Repositories
{
    public class UnitOfWork(ApplicationDbContext AASTHA2Context) : IUnitOfWork
    {
        private readonly ApplicationDbContext _AASTHA2Context = AASTHA2Context;
        private IUserRepository _user;
        private IPatientRepository _patient;
        private IOpdRepository _opd;
        private IIpdRepository _ipd;
        private ILookupRepository _lookup;
        private IIpdLookupRepository _ipdLookup;
        private IDeliveryRepository _delivery;
        private IOperationRepository _operation;
        private IChargeRepository _charge;
        private IAppointmentRepository _appointment;
        private IPrescriptionRepository _prescription;
        private IPrescriptionMedicineRepository _prescriptionMedicine;
        private IFormFRepository _formF;
        private IPaymentRepository _payment;
        public IUserRepository Users
        {
            get
            {
                _user ??= new UserRepository(_AASTHA2Context);
                return _user;
            }
        }
        public IPatientRepository Patients
        {
            get
            {
                _patient ??= new PatientRepository(_AASTHA2Context);
                return _patient;
            }
        }
        public IOpdRepository Opds
        {
            get
            {
                _opd ??= new OpdRepository(_AASTHA2Context);
                return _opd;
            }
        }
        public IIpdRepository Ipds
        {
            get
            {
                _ipd ??= new IpdRepository(_AASTHA2Context);
                return _ipd;
            }
        }
        public IIpdLookupRepository IpdLookups
        {
            get
            {
                _ipdLookup ??= new IpdLookupRepository(_AASTHA2Context);
                return _ipdLookup;
            }
        }
        public ILookupRepository Lookups
        {
            get
            {
                _lookup ??= new LookupRepository(_AASTHA2Context);
                return _lookup;
            }
        }
        public IDeliveryRepository Deliveries
        {
            get
            {
                _delivery ??= new DeliveryRepository(_AASTHA2Context);
                return _delivery;
            }
        }
        public IOperationRepository Operations
        {
            get
            {
                _operation ??= new OperationRepository(_AASTHA2Context);
                return _operation;
            }
        }
        public IChargeRepository Charges
        {
            get
            {
                _charge ??= new ChargeRepository(_AASTHA2Context);
                return _charge;
            }
        }
        public IAppointmentRepository Appointments
        {
            get
            {
                _appointment ??= new AppointmentRepository(_AASTHA2Context);
                return _appointment;
            }
        }

        public IPrescriptionRepository Prescriptions
        {
            get
            {
                _prescription ??= new PrescriptionRepository(_AASTHA2Context);
                return _prescription;
            }
        }
        public IPrescriptionMedicineRepository PrescriptionMedicines
        {
            get
            {
                _prescriptionMedicine ??= new PrescriptionMedicineRepository(_AASTHA2Context);
                return _prescriptionMedicine;
            }
        }
        public IFormFRepository FormFDetails
        {
            get
            {
                _formF ??= new FormFRepository(_AASTHA2Context);
                return _formF;
            }
        }
        public IPaymentRepository Payments
        {
            get
            {
                _payment ??= new PaymentRepository(_AASTHA2Context);
                return _payment;
            }
        }
        public void SaveChanges()
        {
            _AASTHA2Context.SaveChanges();
        }
    }
}
