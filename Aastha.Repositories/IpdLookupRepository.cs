﻿using Aastha.Entities.Data;
using Aastha.Entities.Models;
using Aastha.Repositories.Interfaces;

namespace Aastha.Repositories
{
    public class IpdLookupRepository : RepositoryBase<IpdLookup>, IIpdLookupRepository
    {
        public IpdLookupRepository(ApplicationDbContext AASTHA2Context)
            : base(AASTHA2Context)
        {
        }
    }
}
