﻿using Aastha.Entities.Data;
using Aastha.Entities.Models;
using Aastha.Repositories.Interfaces;

namespace Aastha.Repositories
{
    public class OperationRepository : RepositoryBase<Operation>, IOperationRepository
    {
        public OperationRepository(ApplicationDbContext AASTHA2Context)
            : base(AASTHA2Context)
        {
        }
    }
}
