﻿using Aastha.Entities.Data;
using Aastha.Entities.Models;
using Aastha.Entities.StoredProcedure;
using Aastha.Repositories.Interfaces;
using StoredProcedureEFCore;
using System.Linq;
using System.Linq.Dynamic.Core;

namespace Aastha.Repositories
{
    public class PatientRepository : RepositoryBase<Patient>, IPatientRepository
    {
        public PatientRepository(ApplicationDbContext AASTHA2Context)
            : base(AASTHA2Context)
        {
        }
        public IEnumerable<Sp_GetStatistics_Result> GetStatistics(int? Year)
        {
            IEnumerable<Sp_GetStatistics_Result> rows = null;
            _AASTHA2Context.LoadStoredProc("GetPatientStatistics").Exec(r => rows = r.ToList<Sp_GetStatistics_Result>());
            if (Year > 0)
                rows = rows.Where(m => m.Year == Year);
            return rows;
        }
        public IEnumerable<Sp_GetPatientsHistories> GetPatientsHistories(DateTime? startDate, DateTime? endDate, int? month, int? year, int? patientId)
        {
            IEnumerable<Sp_GetPatientsHistories> rows = null;
            _AASTHA2Context.LoadStoredProc("GetPatientsHistories").Exec(r => rows = r.ToList<Sp_GetPatientsHistories>());
            if (startDate != null)
                rows = rows.Where(m => Convert.ToDateTime(m.Date).Date >= startDate && Convert.ToDateTime(m.Date).Date <= endDate);
            if (month > 0 && year > 0)
                rows = rows.Where(m => Convert.ToDateTime(m.Date).Month == month && Convert.ToDateTime(m.Date).Year == year);
            if (patientId > 0)
                rows = rows.Where(m => m.PatientId == patientId);
            return rows;
        }
    }
}
