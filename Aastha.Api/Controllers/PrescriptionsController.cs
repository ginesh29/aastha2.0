﻿using Aastha.Common;
using Aastha.Services;
using Aastha.Services.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Aastha.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PrescriptionsController : ControllerBase
    {
        private static PrescriptionService _prescriptionService;
        public PrescriptionsController(ServicesWrapper ServicesWrapper)
        {
            _prescriptionService = ServicesWrapper.PrescriptionService;
        }
        // GET: api/Prescriptions
        [HttpGet]
        public ActionResult GetPrescriptions([FromQuery] FilterModel filterModel)
        {
            var data = _prescriptionService.GetPrescriptions(filterModel);
            return Ok(data);
        }

        // GET: api/Prescriptions/5
        [HttpGet("{id}")]
        public ActionResult<PrescriptionDTO> GetPrescription(long id, string filter, string includeProperties)
        {
            var prescription = _prescriptionService.GetPrescription(id, filter, includeProperties);
            if (prescription == null)
            {
                return NotFound();
            }
            return prescription;
        }
        [HttpPost]
        public ActionResult<PrescriptionDTO> PostPrescription(PrescriptionDTO prescriptionDTO, string includeProperties = "")
        {            
            _prescriptionService.PostPrescription(prescriptionDTO);
            var prescription = _prescriptionService.GetPrescription(prescriptionDTO.Id, null, includeProperties);
            return CreatedAtAction("GetPrescription", new { prescriptionDTO.Id }, prescription);
        }
        [HttpPut]
        public ActionResult<PrescriptionDTO> PutPrescription(PrescriptionDTO prescriptionDTO, string includeProperties = "")
        {
            var prescription = _prescriptionService.GetPrescription(prescriptionDTO.Id);
            if (prescription == null)
            {
                return NotFound();
            }
            _prescriptionService.PutPrescription(prescriptionDTO);
            prescription = _prescriptionService.GetPrescription(prescriptionDTO.Id, null, includeProperties);
            return CreatedAtAction("GetPrescription", new { prescriptionDTO.Id }, prescription);
        }
        [HttpDelete("{id}")]
        public ActionResult<PrescriptionDTO> DeletePrescription(long id, bool isDeleted, bool removePhysical = false)
        {
            var prescription = _prescriptionService.GetPrescription(id, "");
            prescription.IsDeleted = isDeleted;
            if (prescription == null)
            {
                return NotFound();
            }
            _prescriptionService.RemovePrescription(prescription, "", removePhysical);
            return CreatedAtAction("GetPrescription", new { id }, prescription);
        }
    }
}
