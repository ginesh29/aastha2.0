﻿using Aastha.Common;
using Aastha.Services;
using Aastha.Services.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Aastha.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PaymentsController : ControllerBase
    {
        private static PaymentService _PaymentService;
        public PaymentsController(ServicesWrapper ServicesWrapper)
        {
            _PaymentService = ServicesWrapper.PaymentService;
        }
        // GET: api/Payments
        [HttpGet]
        public ActionResult GetPayments([FromQuery] FilterModel filterModel)
        {
            var result = _PaymentService.GetPayments(filterModel);
            return Ok(result);
        }

        // GET: api/Payments/5
        [HttpGet("{id}")]
        public ActionResult<PaymentDTO> GetPayment(long id, string filter, string includeProperties)
        {
            var Payment = _PaymentService.GetPayment(id, filter, includeProperties);

            if (Payment == null)
            {
                return NotFound();
            }
            return Payment;
        }
        [HttpPost]
        public ActionResult<PaymentDTO> PostPayment(PaymentDTO PaymentDTO, string includeProperties = "")
        {
            _PaymentService.PostPayment(PaymentDTO);
            var Payment = _PaymentService.GetPayment(PaymentDTO.Id, null, includeProperties);
            return CreatedAtAction("GetPayment", new { PaymentDTO.Id }, Payment);
        }
        [HttpPut]
        public ActionResult<PaymentDTO> PutPayment(PaymentDTO PaymentDTO, string includeProperties = "")
        {
            var Payment = _PaymentService.GetPayment(PaymentDTO.Id);
            if (Payment == null)
            {
                return NotFound();
            }
            _PaymentService.PutPayment(PaymentDTO);
            Payment = _PaymentService.GetPayment(PaymentDTO.Id, null, includeProperties);
            return CreatedAtAction("GetPayment", new { PaymentDTO.Id }, Payment);
        }
        [HttpDelete("{id}")]
        public ActionResult<PaymentDTO> DeletePayment(long id, bool isDeleted, bool removePhysical = false)
        {
            var Payment = _PaymentService.GetPayment(id);
            Payment.IsDeleted = isDeleted;
            if (Payment == null)
            {
                return NotFound();
            }
            _PaymentService.RemovePayment(Payment, removePhysical);
            return CreatedAtAction("GetPayment", new { id }, Payment);
        }
    }
}
