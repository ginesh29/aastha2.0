﻿using Aastha.Common;
using Aastha.Services;
using Aastha.Services.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Aastha.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UsersController : ControllerBase
    {
        private static UserService _userService;
        public UsersController(ServicesWrapper ServicesWrapper)
        {
            _userService = ServicesWrapper.UserService;
        }
        // GET: api/Users
        [HttpGet]
        public ActionResult GetUsers([FromQuery] FilterModel filterModel)
        {
            var result = _userService.GetUsers(filterModel);
            return Ok(result);
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public ActionResult<UserDTO> GetUser(long id, string filter)
        {
            var user = _userService.GetUser(id, filter);
            if (user == null)
            {
                return NotFound();
            }
            return user;
        }
        [HttpPost]
        public ActionResult<UserDTO> PostUser(UserDTO userDTO, string includeProperties = "")
        {
            _userService.PostUser(userDTO);
            var user = _userService.GetUser(userDTO.Id, null, includeProperties);
            return CreatedAtAction("GetUser", new { userDTO.Id }, user);
        }
        [HttpPut]
        public ActionResult<UserDTO> PutUser(UserDTO userDTO, string includeProperties = "")
        {
            var user = _userService.GetUser(userDTO.Id);
            if (user == null)
            {
                return NotFound();
            }
            _userService.PutUser(userDTO);
            user = _userService.GetUser(userDTO.Id, null, includeProperties);
            return CreatedAtAction("GetUser", new { userDTO.Id }, user);
        }
        [HttpPost("ChangeFormFPassword")]
        public ActionResult<UserDTO> ChangeFormFPassword(ChangePasswordModel model)
        {
            var user = _userService.VerifyFormF(new LoginModel
            {
                Username = model.Username,
                Password = model.OldPassword,
            });
            if (user == null)
                return Unauthorized(new { message = "Old Password not matched." });
            _userService.ChangeFormFPassword(model.UserId, model.Password);
            return Ok(user);
        }
        [HttpDelete("{id}")]
        public ActionResult<UserDTO> DeleteUser(long id, bool isDeleted, bool removePhysical = false)
        {
            var user = _userService.GetUser(id, "");
            user.IsDeleted = isDeleted;
            if (user == null)
            {
                return NotFound();
            }
            _userService.RemoveUser(user, "", removePhysical);
            return CreatedAtAction("GetUser", new { id }, user);
        }
    }
}
