﻿using Aastha.Common;
using Aastha.Common.Helpers;
using Aastha.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Aastha.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        public IConfiguration Configuration { get; }
        private static UserService _UserService;
        public AuthController(IConfiguration configuration, ServicesWrapper ServicesWrapper)
        {
            Configuration = configuration;
            _UserService = ServicesWrapper.UserService;
        }
        // GET api/values          
        [HttpPost]
        public ActionResult GenerateToken(LoginModel loginModel)
        {
            loginModel.Password = PasswordHash.GenerateHash(loginModel.Password);
            var user = _UserService.VerifyUser(loginModel);

            if (user != null)
            {
                var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]));
                var signinCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
                var claims = new List<Claim>
                {
                    new("UserId", user.Id.ToString()),
                    new("Role", user.IsSuperAdmin ? ((int)RoleEnum.Admin).ToString() : ((int)RoleEnum.Assistant).ToString())
                };
                var tokenOptions = new JwtSecurityToken(
                            issuer: Configuration["Jwt:Issuer"],
                            audience: Configuration["Jwt:Audience"],
                            claims: claims,
                            expires: loginModel.RememberMe ? DateTime.Now.AddMonths(1) : DateTime.Now.AddDays(1),
                            signingCredentials: signinCredentials
                        );
                var tokenString = new JwtSecurityTokenHandler().WriteToken(tokenOptions);
                return Ok(new { Token = tokenString });
            }
            else
                return Unauthorized("Enter valid credential");
        }
        [HttpPost]
        public ActionResult FormFLogin(LoginModel loginModel)
        {
            var user = _UserService.VerifyFormF(loginModel);
            if (user != null)
                return Ok(user);
            else
                return Unauthorized("Enter valid credential");
        }
    }
}