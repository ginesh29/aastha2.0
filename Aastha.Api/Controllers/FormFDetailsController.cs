﻿using Aastha.Common;
using Aastha.Services;
using Aastha.Services.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Aastha.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FormFDetailsController : ControllerBase
    {
        private static FormFService _FormFService;
        public FormFDetailsController(ServicesWrapper ServicesWrapper)
        {
            _FormFService = ServicesWrapper.FormFService;
        }
        // GET: api/FormFs
        [HttpGet]
        public ActionResult GetFormFs([FromQuery] FilterModel filterModel)
        {
            var result = _FormFService.GetFormFDetails(filterModel);
            return Ok(result);
        }

        // GET: api/FormFs/5
        [HttpGet("{id}")]
        public ActionResult<FormFDetailDTO> GetFormF(long id, string filter)
        {
            var FormF = _FormFService.GetFormFDetail(id, filter);

            if (FormF == null)
            {
                return NotFound();
            }
            return FormF;
        }
        [HttpPost]
        public ActionResult<FormFDetailDTO> PostFormF(FormFDetailDTO FormFDetailDTO, string includeProperties = "")
        {
            _FormFService.PostFormFDetail(FormFDetailDTO);
            var FormF = _FormFService.GetFormFDetail(FormFDetailDTO.Id, null, includeProperties);
            return CreatedAtAction("GetFormF", new { FormFDetailDTO.Id }, FormF);
        }
        [HttpPut]
        public ActionResult<FormFDetailDTO> PutFormF(FormFDetailDTO FormFDetailDTO, string includeProperties = "")
        {
            var FormF = _FormFService.GetFormFDetail(FormFDetailDTO.Id);
            if (FormF == null)
            {
                return NotFound();
            }
            _FormFService.PutFormFDetail(FormFDetailDTO);
            FormF = _FormFService.GetFormFDetail(FormFDetailDTO.Id, null, includeProperties);
            return CreatedAtAction("GetFormF", new { FormFDetailDTO.Id }, FormF);
        }
        [HttpDelete("{id}")]
        public ActionResult<FormFDetailDTO> DeleteFormF(long id, bool isDeleted, bool removePhysical = false)
        {
            var FormF = _FormFService.GetFormFDetail(id);
            FormF.IsDeleted = isDeleted;
            if (FormF == null)
            {
                return NotFound();
            }
            _FormFService.RemoveFormFDetail(FormF, "");
            return CreatedAtAction("GetFormF", new { id }, FormF);
        }
        [HttpPut("{id}")]
        public ActionResult<FormFDetailDTO> UpdateFormFStatus(long id)
        {
            var FormF = _FormFService.GetFormFDetail(id);
            FormF.Submitted = true;
            if (FormF == null)
            {
                return NotFound();
            }
            _FormFService.PutFormFDetail(FormF);
            return CreatedAtAction("GetFormF", new { id }, FormF);
        }
    }
}
