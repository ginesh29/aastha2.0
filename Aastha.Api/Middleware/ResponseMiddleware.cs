﻿using Aastha.Api.Models;
using Aastha.Common;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace Aastha.Api.Middleware
{
    public class ResponseMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ResponseMiddleware> _logger;

        public ResponseMiddleware(RequestDelegate next, ILogger<ResponseMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            var originalBodyStream = context.Response.Body;

            using var memoryStream = new MemoryStream();
            context.Response.Body = memoryStream;

            try
            {
                await _next(context);

                memoryStream.Seek(0, SeekOrigin.Begin);
                string responseBody = await new StreamReader(memoryStream).ReadToEndAsync();

                context.Response.Body = originalBodyStream;

                object objResult = null;
                string message = string.Empty;
                object validation = null;
                object error = null;
                var status = context.Response.StatusCode;
                var method = context.Request.Method;

                if (!string.IsNullOrWhiteSpace(responseBody))
                {
                    objResult = JsonConvert.DeserializeObject(responseBody);
                }

                switch (status)
                {
                    case (int)HttpStatusCode.NotFound:
                        message = Messages.NO_DATA_FOUND;
                        objResult = null;
                        break;

                    case (int)HttpStatusCode.OK:
                        message = Messages.FETCH_SUCCESS;
                        break;

                    case (int)HttpStatusCode.Created:
                        message = method switch
                        {
                            "POST" => Messages.RECORD_ADD,
                            "PUT" => Messages.RECORD_UPDATE,
                            _ => Messages.RECORD_DELETE
                        };
                        break;

                    case (int)HttpStatusCode.Unauthorized:
                        message = "Unauthorized";
                        _logger.LogWarning("Unauthorized request: {Message}", message);
                        break;

                    case (int)HttpStatusCode.BadRequest:
                        message = Messages.VALIDATION_ERROR;
                        validation = JsonConvert.DeserializeObject<dynamic>(responseBody)?.errors;
                        _logger.LogWarning("Validation errors: {Validation}", validation);
                        break;

                    case (int)HttpStatusCode.InternalServerError:
                        message = Messages.INTERNAL_SERVER_ERROR;
                        error = JsonConvert.DeserializeObject<dynamic>(responseBody)?.Errors;
                        _logger.LogError("Internal server error: {Error}", error);
                        break;
                }

                var result = CommonApiResponse.Create((HttpStatusCode)status, objResult, message, validation, error);
                context.Response.ContentType = "application/json";
                await context.Response.WriteAsync(JsonConvert.SerializeObject(result));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occurred in ResponseMiddleware.");
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                var errorResponse = CommonApiResponse.Create(HttpStatusCode.InternalServerError, null, Messages.INTERNAL_SERVER_ERROR);
                await context.Response.WriteAsync(JsonConvert.SerializeObject(errorResponse));
            }
            finally
            {
                context.Response.Body = originalBodyStream;
            }
        }
    }

    public static class ResponseWrapperExtensions
    {
        public static IApplicationBuilder UseResponseWrapper(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ResponseMiddleware>();
        }
    }
}
