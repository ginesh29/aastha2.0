CREATE PROCEDURE [dbo].[GetIpdStatistics]
AS
BEGIN
    SET NOCOUNT ON;
    SELECT DATEPART(MONTH, DischargeDate)[Month],
     DATENAME(MONTH,DischargeDate)[MonthName],
     DATEPART(YEAR, DischargeDate)[Year],
     COUNT(*)TotalPatient,
     SUM(ISNULL(Amount,0) - ISNULL(Discount,0))TotalCollection 
    FROM 
     (SELECT DISTINCT IpdId,SUM(Days*Rate)Amount,IsDeleted  FROM Charges
     WHERE IsDeleted IS NULL or IsDeleted=0
     GROUP BY IpdId,IsDeleted)charge
     INNER JOIN 
     (SELECT Id,DischargeDate,Discount FROM Ipds WHERE IsDeleted IS NULL or IsDeleted=0)ipd
     ON charge.IpdId = ipd.Id
    GROUP BY DATEPART(YEAR, DischargeDate),DATEPART(MONTH,  DischargeDate),DATENAME(MONTH,DischargeDate)
    ORDER BY DATEPART(YEAR, DischargeDate) DESC,DATEPART(MONTH,  DischargeDate)
END
GO

CREATE PROCEDURE [dbo].[GetOpdStatistics]
AS
BEGIN
    SET NOCOUNT ON;
    SELECT DATEPART(MONTH,[Date])[Month],
     DATENAME(MONTH,[Date])[MonthName],
     DATEPART(YEAR, [Date])[Year],
     COUNT(*)TotalPatient,
     SUM(ISNULL(ConsultCharge,0) + ISNULL(UsgCharge,0) + ISNULL(UptCharge,0) + ISNULL(InjectionCharge,0) + ISNULL(OtherCharge,0))TotalCollection
    FROM Opds WHERE IsDeleted IS NULL or IsDeleted=0
    GROUP BY DATEPART(YEAR, [Date]),DATEPART(MONTH,  [Date]),DATENAME(MONTH,[Date])
    ORDER BY DATEPART(YEAR, [Date]) DESC,DATEPART(MONTH,  [Date])
END
GO

CREATE PROCEDURE [dbo].[GetPatientStatistics]
AS
BEGIN
    SET NOCOUNT ON;
    SELECT 
     NEWID() Id,
     DATEPART(MONTH, CreatedDate)[Month],
     DATENAME(MONTH,CreatedDate)[MonthName],
     DATEPART(YEAR, CreatedDate)[Year],
     COUNT(*)TotalPatient,
        0.00 TotalCollection
    FROM Patients
    GROUP BY DATEPART(YEAR, CreatedDate),DATEPART(MONTH,  CreatedDate),DATENAME(MONTH,CreatedDate)
    ORDER BY DATEPART(YEAR, CreatedDate) DESC,DATEPART(MONTH,  CreatedDate)
END
GO

CREATE PROCEDURE [dbo].[GetPatientsHistories]
AS
BEGIN
	SET NOCOUNT ON;
	select lst.Id,PatientId,[Type],FORMATMESSAGE('%s %s %s %s',trim(p.Firstname),trim(p.Middlename),ISNULL('('+TRIM(p.Fathername)+')',''),trim(p.Lastname)) PatientName,[Date] from (select Id,PatientId,1 [Type], [Date] from Opds where IsDeleted is null or IsDeleted !=1
	Union All
	select Id,PatientId,2 [Type], DischargeDate [Date] from Ipds where IsDeleted is null or IsDeleted !=1
	Union All
	select Id,PatientId,3 [Type],[Date] from Prescriptions where IsDeleted is null or IsDeleted !=1)lst
	join Patients p on p.Id = lst.PatientId
	order by [Type] asc,[Date] desc
END
GO

CREATE PROCEDURE [dbo].[DeletePrescriptionMedicines]
	@prescriptionId int
AS
BEGIN
	SET NOCOUNT ON;
	delete from PrescriptionMedicines where PrescriptionId = @prescriptionId 
END